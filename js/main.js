
var typingTimer;             
var doneTypingInterval = 2000; 
var $inputName = $('#exampleInputName');
var $inputEmail = $('#exampleInputEmail');

$inputName.on('keyup', function () {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

$inputName.on('keydown', function () {
    clearTimeout(typingTimer);
});

$inputEmail.on('keyup', function () {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

$inputEmail.on('keydown', function () {
    clearTimeout(typingTimer);
});

function doneTyping () {
    valid = checkForm();

    if(!valid) {
        //do nothing
    }else {
        $("#submitForm").attr('disabled', false);
    }
}

$('#submitForm').click(function(){

    valid = checkForm();

    if(!valid) {
        // alert('Błędny w formularzu');
    }else {
        alert('Formularz wysłany!');
    }
    
});

function checkForm(){
    var inputName = $('#exampleInputName').val();
    var inputEmail = $('#exampleInputEmail').val();

    if(inputName == ''){
        return false;
    }

    if(inputEmail == ''){
        console.log('fals mai');
        return false;
    }else{
        return validateEmail(inputEmail);
    }

    return true;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}
